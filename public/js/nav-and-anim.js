/* RECUP LE BTN HAM ET AJOUTE UN EVENT */

const ham_btn = document.getElementById('btn_js')
ham_btn.addEventListener('click', affichageNav)
ham_btn.addEventListener('mouseover', colorFunctionHover)
ham_btn.addEventListener('mouseout', colorFunctionOut)


var color_principal = "#2B2B42"
var color_secondaire = "#FFFFFF"

/* RECUP LA DIV DE LA NAV */
let nav = document.getElementById('navbar')

/* RECUP LES LIEN DE LA NAV ET AJOUT D'UN EVENT */
let link_js_close = document.querySelectorAll('.link_js_close')
link_js_close.forEach( element => 

    element.addEventListener('click', affichageNav) 

)

/* SET LE BACKGROUND */
let back = document.getElementById('nav-back-rounded')
back.style = "border-bottom-left-radius: 100%; border-bottom-right-radius: 100%; height: 0px;";

let content_nav = document.getElementById('content-nav')


/* ETAT  DE LA NAV */
let etat = "fermé"


function affichageNav(){

    if( etat != "open"){

        /* DISPLAY FLEX LA NAV POUR AFFICHER LES LIENS */

        nav.style.display = "flex"  


        /* AFFICHE LE BACKGROUND */
        back.style = "border-bottom-left-radius: 0%; border-bottom-right-radius: 0%; height: 100vh;";
        
        /* AFFICHE LA NAV */
        nav.style.opacity = "100"
        nav.style.zIndex = "9998"

        content_nav.style.zIndex = "9997"
        content_nav.style.transition = "0s"

        /* CHANGE L'ETAT DE LA NAV */
        etat = "open"
        
        
    }else{
        
        /* DISPLAY NONE LA NAV POUR NE PLUS AVOIR LES LIENS SUR LA PAGE */
        nav.style.display = "none"      

        /* MASQUE LE BACKGROUND */
        back.style = "border-bottom-left-radius: 100%; border-bottom-right-radius: 100%; height: 0px;";

        /* MASQUE LA NAV */
        nav.style.opacity = 0      
        nav.style.zIndex = "-9998"

        content_nav.style.zIndex = "-9998"
        content_nav.style.transition = "1.5s"



        /* CHANGE L'ETAT DE LA NAV */
        etat = "fermé"


    }

}

function colorFunctionHover(){

    if( etat != "open"){
        
        ham_btn.style.backgroundColor = color_secondaire
        ham_btn.style.color = color_principal

    }else{
        
        ham_btn.style.backgroundColor = color_principal
        ham_btn.style.color = color_secondaire
    }
}


function colorFunctionOut(){

    if( etat != "open"){
        
        ham_btn.style.backgroundColor = color_principal
        ham_btn.style.color = color_secondaire
        
    }else{
        
        ham_btn.style.backgroundColor = color_secondaire
        ham_btn.style.color = color_principal

    }
}

colorFunctionOut()


/* ADD  */

var competences = document.getElementById('competences')
competences.addEventListener('mouseover', ColorFunctionAddHover )
competences.addEventListener('mouseout', ColorFunctionAddOut )

var contact = document.getElementById('contact')
contact.addEventListener('mouseover', ColorFunctionAddHover )
contact.addEventListener('mouseout', ColorFunctionAddOut )

function ColorFunctionAddHover(){

    ham_btn.style.backgroundColor = color_secondaire
    ham_btn.style.color = color_principal

}

function ColorFunctionAddOut(){

    ham_btn.style.backgroundColor = color_principal
    ham_btn.style.color = color_secondaire

}
var lienEcran = document.querySelectorAll(".lien-pour-js")
var dataLien = []

lienEcran.forEach(link => {

    dataLien.push(link.dataset.lien)
    link.addEventListener('click', function() {
    
        for (i = 0; i < dataLien.length; i++) {
    
            if (dataLien[i] ==  this.dataset.lien) {
                document.querySelector("#" +  this.dataset.lien).scrollIntoView({
                    block: "start", 
                    inline: 'start',  
                    behavior: 'smooth' 
                })
            }
        }
    })
})